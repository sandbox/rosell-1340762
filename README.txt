
-----------------------
  OVERVIEW
-----------------------
The Usertags module allows users to tag content individually, and see what 
other users tagged content as. The functionality is available directly on the 
content - which can be nodes, comments and users. 

To the user, this may for example look like this:
Your tags on this article: interesting, philosophy, have read [add tag]
Other people has tagged this article: boring (5), will read later (4)

-----------------------
  INSTALLATION
-----------------------

1. Download the 7.x version of the Usertags module and place its directory into
your Drupal modules directory.

2. Enable the module by navigating to:

     Administer > Site Building > Modules

   You need to enable at least one widget. I recommend you start by enabling all
   widgets

3. Create a Tag Collection

     Administer > Structure > Usertags > Tag Collections

4. Create a Widget

     Administer > Structure > Usertags > Widgets

   4.1. Give it a name and a label
   4.2. Select which Tag Collection the widget should "be connected to". The 
        widget will display tags from this collection, and add tags to it, when
        a user adds a tag. Its thus possible to several widgets connected to
        the same tag collection. When a tag is added, all widgets that are
        connected to the tag collection will be updated
   4.3  Select widget type. I recommend you start by selecting "freetagging".

        Freetagging:
        ------------
        Freetagging allows users to add tags freely. Here are some examples of
        how it may look for the end-user:

        Your tags on this article: interesting, philosophy, have read [add tag]
        This movie makes me: cry, bored, smile, skitzofrenic [add feeling]


        Community:
        ------------
        Summarizes the tags that people have added to the node. Examples:

        Other people has tagged this article: boring (5), will read later (4)
        This movie makes the community: sad(98), desperate(62) [add feeling]

        It can be used in conjuction with freetagging, to for example display:

        Your tags on this article: interesting, philosophy, have read [add tag]
        People has tagged this article: boring (5), will read later (4)

        But it can also be used alone, if focus is on the collaboration on 
        tagging content, rather than on the individual user. Ie:

        Movie genre: action (98), comedy (74), thriller (72) [add genre]


        Checkboxes and radios:
        ---------------------
        This widget alows users to tag content with predefined tags. The
        The options that are displayed are those tags that are already defined
        in the tag collection. Now, there is a difference between a tag beeing
        defined, and beeing used. Tags can have a use-count of 0. Tags can be
        defined in two ways. 1) When a user adds a tag (ie with the freetagging
        widget), which does not already exist. 2) By going to the
        tag-collection tagcollection and clicking "list tags". You will
        typically use the second approach, when defining tags for use in this
        widget. Its easier, AND it allows you to order the tags. If you order
        the tags in the tag-collection, remember to choose to order by weight
        in the widget (in the "ordering of tags" option)

        
        Dropdown:
        ---------------------
        Similar to "Checkboxes and radios" widget. The main difference is that
        the first option in the select corresponds to no tags. You can change 
        the text of this first option, and thus make it seem to the user like 
        he/she has selected something. Ie, you could call the widget "Do you
        like this icecream?", only define one tag in the tag collection: "No", 
        and then set the text of the no-tags-selected option to "Yes". To the 
        user it will seem like the field has a default option. There is a 
        drawback of doing this, though: Its not so straightforward to list the
        icecreams that the user likes, as the user hasn't any tags on these.
        Thus, you will typically set the first option to "Choose" or something
        like that.


   4.4  Select which content the widget should be displayed on. For example
        "Article" and "User". If you want the widget to display on both
        "Article" and "User", but want the labels to be different, simply 
        create two widgets, one for each scenario - and set them to connect to 
        the same Tag Collection.

   4.5  Select which view modes the widget should be displayed on. For example
        "Full content", "Teaser" and "RSS". The widget will only display, when
        content is viewed in this view-mode AND the content is of one of the
        types selected in 4.4

   4.6  Select weight. The weight determines the position of the widget inside
        the content. Heavy weight means it will be positioned further down

5. Start tagging!

6. The module supplies two blocks for each tag collection. One is for showing
   all tags in the collection, the other is for showing the users tags in the
   collection. These can be added, once you have at least one Tag Collection.
   Go to:

     Administer > Structure > Blocks

   PS: Blocks are not automatically updated, when a tag collection changes. This
   may be implemented in the future, though

7. The module has Views integration. In fact the module uses views in order
   to generate the lists that shows, when you click on a tag. In order to change 
   the output, you can simply override the default views.

   The default views are:
   - Usertagged comments (any user)
   - Usertagged comments (current user)
   - Usertagged nodes (any user)
   - Usertagged nodes (current user)
   - Usertagged users (any user)
   - Usertagged users (current user)

   If you wish to change what is shown on usertags/stuff-you-tagged/1, you should
   know, that it is generated by appending the output of these three views:
   - Usertagged comments (current user)
   - Usertagged nodes (current user)
   - Usertagged users (current user)

   Similarily, usertags/stuff-somebody-tagged/1, is generated by an appendment of
   these three views:
   - Usertagged comments (any user)
   - Usertagged nodes (any user)
   - Usertagged users (any user)

   It is also possible to generate lists of tags. No default views are supplied
   for this, though

8. NOTE: This is a beta. Its pretty stable, but there is STUFF YOU SHOULDN'T DO:
   - Don't delete a tag collection that is in use (delete the widgets first) 
   - Don't uninstall a widget that is in use (delete the widgets first) 
   - Don't uninstall Usertags without removing any usertag blocks first

-----------------------
  SIMILAR MODULES
-----------------------

Flags:
------
Allows users to flag nodes. Also comes with actions, so flagging triggers some 
action. I will probably extend TagThis to include flag functionality. - In form
of a new widget. I however have no plans to implement actions, so if you want
flags with actions, go for this module.


Community Tags:
---------------
Quite similar to this module. Differences:
- Community Tags is limited to tagging nodes. Usertags can also allows tagging 
  comments and users
- TagThis has several types of widgets and can easily be extended (by a drupal
  programmer). Widgets gives new opportunities. The functionality of the 
  "Option" widget for example not possible with Community tags. In the future, 
  I hope to provide new widgets. For example: "Rating" and "Flag"
- Community Tags integrates with Taxonomy, Usertags introduces its own 
  vocabularies ("tag collections"). There are pros and cons


-----------------------
  KNOWN ISSUES
-----------------------
- Does not work with Display Suite


-----------------------
  AUTHOR INFORMATION
-----------------------

Written by Bjørn Rosell
homepage: www.rosell.dk



