function usertags_checkboxes_and_radios_widget_init(uniqueId) {
  usertags_checkboxes_and_radios_widget_activate(uniqueId);
}

function usertags_checkboxes_and_radios_widget_contentRewritten(uniqueId) {
  usertags_checkboxes_and_radios_widget_activate(uniqueId);
}


function usertags_checkboxes_and_radios_widget_activate(uniqueId) {
  $ = jQuery;

  var inputs = $('input:radio[name=usertags_radio_' + uniqueId + ']');
  if (inputs.length == 0) {
    inputs = $('input:checkbox[name=usertags_checkbox_' + uniqueId + ']');
  }

  if (inputs.length > 0) {
//    inputs.wrap('<span></span').parent().css({background:"yellow", border:"3px red solid"});
    var anonAlertMsg = $('div#usertags_checkboxes_and_radios_' + uniqueId + ' div.usertags-checkboxes-and-radios-anon-alert-message');
    if (anonAlertMsg.length > 0) {
      inputs.change(function() {
        alert(anonAlertMsg[0].innerHTML);
        this.checked = !(this.checked);
      });
    }
    else {
      inputs.change(function() {
        var arrayOfTagIds = [];
        for (var i=0; i<inputs.length; i++) {
          if (inputs[i].checked) {
            arrayOfTagIds.push(inputs[i].value);
          }
        }
        usertags_command_setTagsForCurrentUserById(uniqueId, arrayOfTagIds);
      });
    }
  }
}


