function usertags_community_widget_init(uniqueId) {
  usertags_community_widget_activate(uniqueId);
}

function usertags_community_widget_contentRewritten(uniqueId) {
  usertags_community_widget_activate(uniqueId);
}


function usertags_community_widget_activate(uniqueId) {
  $ = jQuery;
  var divId = 'usertags_community_' + uniqueId;

  var anonAlertMsg = $('div#usertags_community_' + uniqueId + ' div.usertags-community-anon-alert-message');
  if (anonAlertMsg.length > 0) {
    $('a#usertags_community_' + uniqueId + '_addtag').click(function() {
      alert(anonAlertMsg[0].innerHTML);
    });
  }
  else {
    var anonType = $('div#usertags_community_' + uniqueId + ' div.usertags-community-anon-anonymous');
    if (anonType.length == 0) {
      $('a#usertags_community_' + uniqueId + '_addtag').click(function() {
        var msg = $('div#usertags_community_' + uniqueId + ' div.usertags-community-text-in-prompt')[0].innerHTML;
        var tag = prompt(msg, "");
        if (tag!=null && tag!="") {
          usertags_command_addTagForCurrentUserByName(uniqueId, tag);
        }
      })
    }
  }
}

