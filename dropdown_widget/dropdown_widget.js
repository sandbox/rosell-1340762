function usertags_dropdown_widget_init(uniqueId) {
  usertags_dropdown_widget_activate(uniqueId);
}

function usertags_dropdown_widget_contentRewritten(uniqueId) {
  usertags_dropdown_widget_activate(uniqueId);
}


function usertags_dropdown_widget_activate(uniqueId) {
  $ = jQuery;

  var select = $('#usertags_dropdown_select_' + uniqueId);
  if (select.length > 0) {
    var anonAlertMsg = $('div#usertags_dropdown_' + uniqueId + ' div.usertags-dropdown-anon-alert-message');
    if (anonAlertMsg.length > 0) {
      select.change(function() {
        alert(anonAlertMsg[0].innerHTML);
        this.selectedIndex = 0;
      });
    }
    else {
      select.change(function() {
        var tid = select.val();
        var arrayOfTagIds = [];
        if (tid != 0) {
          arrayOfTagIds.push(select.val());
        }
        usertags_command_setTagsForCurrentUserById(uniqueId, arrayOfTagIds);
      });
    }
  }
}


