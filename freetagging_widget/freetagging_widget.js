function usertags_freetagging_widget_init(uniqueId) {
  usertags_freetagging_widget_activate(uniqueId);
}

function usertags_freetagging_widget_contentRewritten(uniqueId) {
  usertags_freetagging_widget_activate(uniqueId);
}

function usertags_freetagging_widget_activate(uniqueId) {
//  alert('activate:' + uniqueId)
  $ = jQuery;

  var divId = 'usertags_freetagging_' + uniqueId;
  var crossClass = 'usertags-freetagging-' + uniqueId + '-cross';

  $('div#' + divId + ' div.usertags-freetagging-tag').append('<div class="' + crossClass + ' usertags-freetagging-cross">x</div>');

  $('.' + crossClass).hide();

  $('.' + crossClass).click(function() {
    // grab container div (div around usertag - defined in usertags.widget_freetagging.inc)
    // from its id, we can extract the tid  (id is formatted like this: usertags_freetagging_$uniqueId_usertag_$tid
    var usertagDivElm = $(this.parentNode);
    var tid = usertagDivElm.attr('id').split('_').pop();

    usertags_command_removeTagForCurrentUser(uniqueId, tid);
    //$(this.parentNode).fadeOut(500);
  })

  $('div#' + divId + ' div.usertags-freetagging-tag').hover(
    function () {
      $(this).find('.usertags-freetagging-cross').show();
    },
    function () {
      $(this).find('.usertags-freetagging-cross').hide();
    }
  );

  var anonAlertMsg = $('div#usertags_freetagging_' + uniqueId + ' div.usertags-freetagging-anon-alert-message');
  if (anonAlertMsg.length > 0) {
    $('a#usertags_freetagging_' + uniqueId + '_addtag').click(function() {
      alert(anonAlertMsg[0].innerHTML);
    });
  }
  else {
    var anonType = $('div#usertags_freetagging_' + uniqueId + ' div.usertags-freetagging-anon-anonymous');
    if (anonType.length == 0) {
      // TODO: nicer confirm: http://thrivingkings.com/apprise/
      $('a#usertags_freetagging_' + uniqueId + '_addtag').click(function() {
        var msg = $('div#usertags_freetagging_' + uniqueId + ' div.usertags-freetagging-text-in-prompt')[0].innerHTML;
        var tag = prompt(msg, "");
        if (tag!=null && tag!="") {
          usertags_command_addTagForCurrentUserByName(uniqueId, tag);
        }
      })
    }
  }

}

