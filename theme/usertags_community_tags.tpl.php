<?php

// $Id$

/**
 * @file
 * Formats community tags in block
 * 
 * Available variables:
 * - $community_tags: An array of community tags, each having the following key:
 *    - tagname: name of the tag
 *    - num: number of times tag is used by the community
 *    - tid: tag id   
 * - $tag_collection_machine_name: The machine-name of the tag-collection
 */
?>
<div class="usertags-block-communitytags usertags-block-communitytags-<?php print $tag_collection_machine_name ?>">
<?php 
foreach ($community_tags as $community_tag) {
  $linktext = $community_tag['tagname'] . ' ';
//  $linktext = $community_tag['tagname'] . '(' . $community_tag['num'] . ') ';
  $href = 'usertags/stuff-somebody-tagged/' . $community_tag['tid'];
  $options = array(
    'attributes' => array('title' => 'tagged used ' . $community_tag['num'] . ' times'),
  );
  print l($linktext, $href, $options);
}
?>
</div>



