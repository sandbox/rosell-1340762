<?php

// $Id$

/**
 * @file
 * Formats user tags in block
 * 
 * Available variables:
 * - $user_tags: An array of community tags, each having the following key:
 *    - tagname: name of the tag
 *    - num: number of times tag is used
 *    - tid: tag id   
 * - $tag_collection_machine_name: The machine-name of the tag-collection
 */
?>
<div class="usertags-block-usertags usertags-block-usertags-<?php print $tag_collection_machine_name ?>">
<?php 
foreach ($user_tags as $user_tag) {
  $linktext = $user_tag['tagname'] . ' ';
  $href = 'usertags/stuff-you-tagged/' . $user_tag['tid'] . ' (' . $user_tag['num'] . ')';
  print l($linktext, $href);
}
?>
</div>



