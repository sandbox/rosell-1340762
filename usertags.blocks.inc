<?php

/**
 * @file
 * Blocks for usertags
 */

function usertags_block_info() {

  $result = usertags_db_fetch_tag_collections();
  $blocks = array();
  while ($record = $result->fetchAssoc()) {
    $blocks['usertags_community_' . $record['vid']] = array(
      'info' => t('Usertags: Community tags for tag collection: ' . $record['name']),
      'cache' => DRUPAL_NO_CACHE,
    );
    $blocks['usertags_usertags_' . $record['vid']] = array(
      'info' => t('Usertags: User tags for tag collection: ' . $record['name']),
      'cache' => DRUPAL_NO_CACHE,
    );
  }
  return $blocks;
}

/**
 * Implements hook_block_configure().
 */
function usertags_block_configure($delta) {
  if (strpos($delta, 'usertags_community') === 0) {
    $vid = drupal_substr($delta, 19);
    $form['usertags_limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum number of tags to display'),
      '#default_value' => variable_get('usertags_block_community_' . $vid, 30),
      '#size' => '3',
      '#maxlength' => '4',
    );
    return $form;
  }
  if (strpos($delta, 'usertags_usertags') === 0) {
    $vid = drupal_substr($delta, 18);
    $form['usertags_limit'] = array(
      '#type' => 'textfield',
      '#title' => t('Maximum number of tags to display'),
      '#default_value' => variable_get('usertags_block_usertags_' . $vid, 30),
      '#size' => '3',
      '#maxlength' => '4',
    );
    return $form;
  }
}

function usertags_block_save($delta, $edit = array()) {
  if (strpos($delta, 'usertags_community') === 0) {
    $vid = drupal_substr($delta, 19);
    variable_set('usertags_block_community_' . $vid, $edit['usertags_limit']);
  }
  if (strpos($delta, 'usertags_usertags') === 0) {
    $vid = drupal_substr($delta, 18);
    variable_set('usertags_block_usertags_' . $vid, $edit['usertags_limit']);
  }
}

function usertags_block_view($delta) {
  if (strpos($delta, 'usertags_community') === 0) {
    return usertags_block_community_terms(drupal_substr($delta, 19));
  }
  global $user;
  if (strpos($delta, 'usertags_usertags') === 0) {
    $uid = $user->uid;
    // Don't show block for anonymous users
    if ($uid != 0) {
      $vid = drupal_substr($delta, 18);
      $result = usertags_db_fetch_tags_on_entity(NULL, NULL, $vid, $uid, 1);
      // Only show block, if user has tags in the tag collection
      if ($result->fetchAssoc()) {
        return usertags_block_usertags($vid);
      }
    }
  }
}

function usertags_block_community_terms($vid) {
  $rec = usertags_db_get_tag_collection($vid);

  $block['subject'] = t('All tags in tag collection: "@tagcollection"', array('@tagcollection' => $rec['name']));

  $result = usertags_db_fetch_tags_on_entity(NULL, NULL, $vid, NULL, variable_get('usertags_block_community_' . $vid, 30));
  $community_tags = array();

  while ($record = $result->fetchAssoc()) {
    $community_tags[] = array(
      'tagname' => $record['name'],
      'num' => $record['num'],
      'tid' => $record['tid'],
    );
  }

  $machine_name = '';
  $rec = usertags_db_get_tag_collection($vid);
  if ($rec != NULL) {
    $machine_name = $rec['machine_name'];
  }

  $build['usertags_links'] = array('#theme' => 'usertags_community_tags', '#community_tags' => $community_tags, '#tag_collection_machine_name' => $machine_name);

/*
  $build['test'] = array(
    '#type' => 'markup',
    '#markup' => theme('usertags_community_tags', array('community_tags' => $community_tags)),
  );
 */

  $block['content'] = $build;
  return $block;
}

function usertags_block_usertags($vid) {
  global $user;
  $uid = $user->uid;

  $rec = usertags_db_get_tag_collection($vid);
  $block['subject'] = t('Your tags in tag collection: "@tagcollection"', array('@tagcollection' => $rec['name']));

  $query = db_select('usertags_index', 'ti');
  $query->join('usertags_tag_data', 'ttd', 'ti.tid = ttd.tid');
  $query->fields('ttd', array('tid', 'name'));
  $query->groupBy('ti.tid');  // GROUP BY term - we only want one record per tag
  $query->condition('ti.uid', $uid, '=');
  $query->condition('ttd.vid', $vid, '=');
  $query->addExpression('COUNT(*)', 'num');  // Get use count for each tag
  $query->orderBy('num', 'DESC');
  $query->orderBy('name', 'ASC');
  $query->range(0, 30);  //LIMIT to 30 records
  $result = $query->execute();

  $user_tags = array();
  while ($record = $result->fetchAssoc()) {
    $user_tags[] = array(
      'tagname' => $record['name'],
      'num' => $record['num'],
      'tid' => $record['tid'],
    );
  }

  $machine_name = '';
  $rec = usertags_db_get_tag_collection($vid);
  if ($rec != NULL) {
    $machine_name = $rec['machine_name'];
  }

  $build['usertags_links'] = array('#theme' => 'usertags_user_tags', '#user_tags' => $user_tags, '#tag_collection_machine_name' => $machine_name);

  $block['content'] = $build;
  return $block;
}






