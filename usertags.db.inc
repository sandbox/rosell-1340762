<?php

/**
 * @file
 * DB api
 */

/**
 *  Fetch tags on entity.
 *  If uid is set, then only tags from that user will be fetched
 *  otherwise tags from all users will be fetched
 */
function usertags_db_fetch_tags_on_entity($etype = NULL, $cid = NULL, $vid, $uid = NULL, $limit = NULL) {

  // PS: Nice example of using db_select here:
  // http://api.drupal.org/api/drupal/includes--database--database.inc/function/db_select/7

  $query = db_select('usertags_index', 'ti');
  $query->join('usertags_tag_data', 'ttd', 'ti.tid = ttd.tid'); //JOIN usertags_index with usertags_tag_data
//  $query->fields('ti');  // SELECT all fields from usertags_index
  $query->fields('ttd', array('tid', 'name'));  // SELECT the 'tid' and 'name' fields from usertags_tag_data
  $query->groupBy('ti.tid');  //GROUP BY term - we only want one record per term
  if (isset($uid)) {
    $query->condition('ti.uid', $uid, '=');  // Only fetch for specified user
  }
  else {
    $query->addExpression('COUNT(*)', 'num');  // Get count for each term
    $query->orderBy('num', 'DESC'); //ORDER BY count
  }
  $query->orderBy('name', 'ASC'); //SUB-ORDER BY name
  if ($etype != NULL) {
    $query->condition('ti.entity_type', $etype, '=');  // Only fetch for specified entity
  }
  if ($cid != NULL) {
    $query->condition('ti.cid', $cid, '=');  // Only fetch for specified entity
  }
  $query->condition('ttd.vid', $vid, '=');  // Only fetch for specified tag collection

  if ($limit != NULL) {
    $query->range(0, $limit);  //LIMIT to x records
  }
  $result = $query->execute();

  return $result;
}

/**
 *  Returns number of tags for a certain user on a certain entity
 */
function usertags_db_count_user_tags($etype, $cid, $vid, $uid) {
  $query = db_select('usertags_index', 'ti');
  $query->join('usertags_tag_data', 'ttd', 'ti.tid = ttd.tid'); //JOIN usertags_index with usertags_tag_data
  $query->condition('ti.entity_type', $etype, '=');  // Only fetch for specified entity
  $query->condition('ti.cid', $cid, '=');  // Only fetch for specified entity
  $query->condition('ttd.vid', $vid, '=');  // Only fetch for specified tag collection
  $query->condition('ti.uid', $uid, '=');  // Only fetch for specified user
  return $query->countQuery()->execute()->fetchField();
}


/**
 *  Fetch terms in tag collection.
 *  Fetches all terms in a tag collection
 */
function usertags_db_fetch_tags_in_tag_collection($vid, $order_by = 'name') {
  $query = db_select('usertags_tag_data', 'ttd');
  $query->fields('ttd', array('tid', 'name', 'weight'));
  $query->condition('ttd.vid', $vid, '=');  // Only fetch terms in specified tag collection
  $query->orderBy($order_by, 'ASC');
  $result = $query->execute();
  return $result;
}

/**
 *  Inserts a row in the usertags_index' table
 */
function usertags_db_insert_row_in_usertags_index($etype, $cid, $uid, $tid) {
  $query = db_insert('usertags_index');
  $query->fields(array(
    'entity_type' => $etype,
    'cid' => $cid,
    'uid' => $uid,
    'tid' => $tid,
    'created' => REQUEST_TIME,
  ));
  $result = $query->execute();
  return $result;
}

/**
 *  Fetch tid from tagname, for a given tag collection
 *  If the tagname isn't in db, then NULL is returned
 */
function usertags_fetch_tid_from_tagname($term, $vid) {
  $query = db_select('usertags_tag_data', 'ttd');
  $query->fields('ttd', array('tid'));
  $query->condition('ttd.name', $term, '=');
  $query->condition('ttd.vid', $vid, '=');
  $result = $query->execute();
  $firstrecord = $result->fetchAssoc();
  return $firstrecord['tid'];
}

/**
 *  Fetch tagname from term id (tid)
 *  If the tid isn't in db, then NULL is returned
 */
function usertags_fetch_tagname_from_tid($tid) {
  $query = db_select('usertags_tag_data', 'ttd');
  $query->fields('ttd', array('name'));
  $query->condition('ttd.tid', $tid, '=');
  $result = $query->execute();
  $firstrecord = $result->fetchAssoc();
  return $firstrecord['name'];
}

/**
 *  Insert a tag directly in a tag collection without checking if its already there
 */
function usertags_insert_tag($term, $vid) {
  $query = db_insert('usertags_tag_data');
  $query->fields(array(
    'name' => $term,
    'vid' => $vid,
  ));
  $result = $query->execute();
  return $result;
}

/**
 *  Check if record exists in {usertags_index}
 */
function usertags_db_check_if_relation_exists($etype, $cid, $uid, $tid) {
  $query = db_select('usertags_index', 'ti');
  $query->fields('ti', array('tid'));
  $query->condition('ti.entity_type', $etype, '=');
  $query->condition('ti.cid', $cid, '=');
  $query->condition('ti.uid', $uid, '=');
  $query->condition('ti.tid', $tid, '=');
  $result = $query->execute();
  $firstrecord = $result->fetchAssoc();
  return ($firstrecord['tid'] == NULL);
}

/**
 *  Insert a tag on a node for a user
 *  Returns tid, or NULL to indicate that tag wasn't inserted (because it already existed)
 */
function usertags_db_insert_tag_on_entity($etype, $cid, $term, $vid, $uid = NULL) {
  global $user;
  if ($uid == NULL) {
    $uid = $user -> uid;
  }

  $tid = usertags_fetch_tid_from_tagname($term, $vid);
  if ($tid == NULL) {
    // If tag doesn't exist, create it!
    usertags_insert_tag($term, $vid);
    $tid = usertags_fetch_tid_from_tagname($term, $vid);
  }

  if (usertags_db_check_if_relation_exists($etype, $cid, $uid, $tid)) {
    usertags_db_insert_row_in_usertags_index($etype, $cid, $uid, $tid);
  }
  else {
    return NULL;
  }
  return $tid;
}

/**
 *  Delete record in {usertags_index}
 */
function usertags_db_delete_record($etype, $cid, $uid, $tid) {
  $query = db_delete('usertags_index');
  $query->condition('entity_type', $etype, '=');
  $query->condition('cid', $cid, '=');
  $query->condition('uid', $uid, '=');
  $query->condition('tid', $tid, '=');
  $num_deleted = $query->execute();
  return $num_deleted;
}


/**
 *  Delete record in {usertags_index}
 */
function usertags_db_delete_usertag($etype, $cid, $uid, $tid) {
  usertags_db_delete_record($etype, $cid, $uid, $tid);
  // TODO: Maybe also remove term, if nobody is using it any longer?
}

/**
 *  Delete all tags on an entity
 */
function usertags_db_delete_all_usertags_on_entity($etype, $cid, $uid) {
  $query = db_delete('usertags_index');
  $query->condition('entity_type', $etype, '=');
  $query->condition('cid', $cid, '=');
  $query->condition('uid', $uid, '=');
  $num_deleted = $query->execute();
  return $num_deleted;
}

/**
 *  Fetch entities (ie nodes, comments, users) with a certain tag
 *  if 'entity_type' is set, only entities of this type will be fetched
 *  If 'uid' is set, then only entities tagged by that user will be fetched
 */
function usertags_db_fetch_entities($tid, $entity_type = NULL, $uid = NULL) {
  $query = db_select('usertags_index', 'ti');
  $query->fields('ti', array('entity_type', 'cid'));
  $query->condition('ti.tid', $tid, '=');  // Only fetch for specified tid
  if ($entity_type != NULL) {
    $query->condition('ti.entity_type', $entity_type, '=');
  }
  if (isset($uid)) {
    $query->condition('ti.uid', $uid, '=');  // Only fetch for specified user
  }
  else {
    // Fetch for all users - Order by popularity
    $query->groupBy('ti.cid'); // GROUP BY term - we only want one record per node
    $query->addExpression('COUNT(*)', 'num');  // Get count for each node
    $query->orderBy('num', 'DESC'); // ORDER BY count
  }
  $result = $query->execute();

  return $result;
}

/**
 *  Fetch tag collection id from widget id
 *  If the widget isn't in db, then NULL is returned
 */
function usertags_fetch_vid_from_eid($eid) {
  $query = db_select('usertags_widget', 'te');
  $query->fields('te', array('vid'));
  $query->condition('te.eid', $eid, '=');
  $result = $query->execute();
  $firstrecord = $result->fetchAssoc();
  return $firstrecord['vid'];
}

// ---------------------
//        Widget
// ---------------------


/**
 *  Fetch all widgets
 */
function usertags_db_fetch_widgets() {

  $query = db_select('usertags_widget', 'te');
  $query->join('usertags_tag_collection', 'tv', 'te.vid = tv.vid');
  $query->fields('te', array('eid', 'name', 'vid', 'widget_type', 'bundles', 'view_modes', 'roles', 'weight', 'label', 'label_style'));  // Im not just fetching all, because of clash with 'name' field
  $query->fields('tv', array('name'));  // SELECT the 'name' field from usertags_tag collection
  $query->orderBy('te.eid', 'ASC'); // ORDER BY widget id
  $result = $query->execute();

  return $result;
}

// -------------------------------------------------------
//         ADMIN - should be moved to an admin.inc
// -------------------------------------------------------


// ---------------------
//        Widget
// ---------------------


/**
 *  Get widget by id (eid)
 *  If the widget isn't in db, then NULL is returned
 */
function usertags_db_get_widget($eid) {
  $query = db_select('usertags_widget', 'te');
  $query->fields('te');   // select all fields
  $query->condition('te.eid', $eid, '=');
  $result = $query->execute();
  $firstrecord = $result->fetchAssoc();
  return $firstrecord;
}

/**
 *  Get widget by machine name
 *  If the widget isn't in db, then NULL is returned
 */
function usertags_db_get_widget_by_machine_name($machine_name) {
  $query = db_select('usertags_widget', 'widget');
  $query->fields('widget');   // select all fields
  $query->condition('widget.machine_name', $machine_name, '=');
  $result = $query->execute();
  $firstrecord = $result->fetchAssoc();
  return $firstrecord;
}


// -----------------------
//        Tag Collection
// -----------------------

/**
 *  Fetch all tag collections
 */
function usertags_db_fetch_tag_collections() {
  $query = db_select('usertags_tag_collection', 'tv');
  $query->fields('tv');  // SELECT all fields
  $query->orderBy('tv.vid', 'ASC'); // ORDER BY id
  $result = $query->execute();
  return $result;
}

/**
 *  Get tag collection by id (vid)
 *  If the tag collection isn't in db, then NULL is returned
 */
function usertags_db_get_tag_collection($vid) {
  $query = db_select('usertags_tag_collection', 'tc');
  $query->fields('tc');   // select all fields
  $query->condition('tc.vid', $vid, '=');
  $result = $query->execute();
  $firstrecord = $result->fetchAssoc();
  return $firstrecord;
}

/**
 *  Get tag collection by machine name
 *  If the tag collection isn't in db, then NULL is returned
 */
function usertags_db_get_tag_collection_by_machine_name($machine_name) {
  $query = db_select('usertags_tag_collection', 'tc');
  $query->fields('tc');   // select all fields
  $query->condition('tc.machine_name', $machine_name, '=');
  $result = $query->execute();
  $firstrecord = $result->fetchAssoc();
  return $firstrecord;
}

/**
 *  Get tag collection name by id (vid)
 *  If the tag collection isn't in db, then an empty string is returned
 */
function usertags_db_get_tag_collection_name($vid) {
  $rec = usertags_db_get_tag_collection($vid);
  if ($rec == NULL) {
    return '';
  }
  return $rec['name'];
}

/**
 *  Delete tag collection
 */
function usertags_db_delete_tag_collection($vid) {
  db_delete('usertags_tag_collection')
    ->condition('vid', $vid)
    ->execute();
}

// -----------------------
//        Tags
// -----------------------

// These have been placed in usertags.tags.inc

