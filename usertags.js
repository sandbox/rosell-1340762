usertags_widgets = [];

usertags_initFunctions = [];

(function ($) {
  $(document).ready(function() {
    var a = usertags_widgets;
    for (var i=0; i<a.length; i++) {

      // run init function for each registed widget
      var functionName = "usertags_" + a[i].widgetType + "_init";
      var uniqueId = a[i].uniqueId;
      window[functionName].call(null, uniqueId);
    }
  })
})(jQuery);


function usertags_registerWidget(widgetType, vid, uniqueId) {
//  usertags_initFunctions.push(["usertags_" + widgetType + "_init", uniqueId]);
  usertags_widgets.push({widgetType: widgetType, vid:vid, uniqueId:uniqueId})
}

function _usertags_dispatchCommand(uniqueId, command) {
  var elm = $('div#usertags_command_' + uniqueId + ' input');
  $(elm).val(command);

  // the blur event will trigger ajax
  $(elm).triggerHandler('blur');
}

function usertags_tag_collectionUpdated(vid, uniqueId) {
  var a = usertags_widgets;
  for (var i=0; i<a.length; i++) {
    if ((a[i].vid == vid) && (a[i].uniqueId != uniqueId)) {
      usertags_command_reload(a[i].uniqueId);
    }
  }
}

function usertags_command_removeTagForCurrentUser(uniqueId, tid) {
  _usertags_dispatchCommand(uniqueId, 'remove_usertag:' + tid);
}

function usertags_command_addTagForCurrentUserByName(uniqueId, tag) {
  _usertags_dispatchCommand(uniqueId, 'add_usertag_by_name:' + tag);
}

function usertags_command_addTagForCurrentUserById(uniqueId, tid) {
  _usertags_dispatchCommand(uniqueId, 'add_usertag_by_id:' + tag);
}

function usertags_command_setTagsForCurrentUserById(uniqueId, arrayOfTagIds) {
  _usertags_dispatchCommand(uniqueId, 'set_usertags_by_id:' + arrayOfTagIds.join(','));
}

function usertags_command_reload(uniqueId) {
  _usertags_dispatchCommand(uniqueId, 'reload');
}

