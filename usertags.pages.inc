<?php

/**
 * @file
 * Non-admin pages
 */

/**
 *  Get page title from tid
 *  $titlepage: either 'title_page_stuff_you_tagged' or 'title_page_stuff_somebody_tagged'
 *  $tid: The tag id
 */
function _usertags_get_page_title($titlepage, $tid) {
  $query = db_select('usertags_tag_data', 'td');
  $query->join('usertags_tag_collection', 'tc', 'td.vid = tc.vid');
  $query->fields('td', array('name'));
  $query->fields('tc', array($titlepage));
  $query->condition('td.tid', $tid, '=');
  $result = $query->execute();
  $firstrecord = $result->fetchAssoc();
  if ($firstrecord != NULL) {
    return str_replace('%tagname', $firstrecord['name'], $firstrecord[$titlepage]);
  }
  return '';
}

function usertags_stuff_you_tagged_title($tid) {
  return _usertags_get_page_title('title_page_stuff_you_tagged', $tid);
}

function usertags_stuff_you_tagged_page($tid) {
  $nodes_html = views_embed_view('usertags_nodes_current_user', 'default', $tid);
  $comments_html = views_embed_view('usertags_comments_current_user', 'default', $tid);
  $users_html = views_embed_view('usertags_users_current_user', 'default', $tid);
  return $nodes_html . $comments_html . $users_html;
}

function usertags_stuff_somebody_tagged_title($tid) {
  return _usertags_get_page_title('title_page_stuff_somebody_tagged', $tid);
}

function usertags_stuff_somebody_tagged_page($tid) {
  $nodes_html = views_embed_view('usertags_nodes_any_user', 'default', $tid);
  $comments_html = views_embed_view('usertags_comments_any_user', 'default', $tid);
  $users_html = views_embed_view('usertags_users_any_user', 'default', $tid);
  return $nodes_html . $comments_html . $users_html;
}


