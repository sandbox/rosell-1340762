<?php

/**
 * @file
 * Tag Collections
 */

// -------------------------------------
//               List
// -------------------------------------

/**
 * Form for tag collections
 */
function usertags_tag_collections_list() {
  $header = array(
    array('data' => t('Tag Collection')),
    array('data' => t('Operations')),
  );

  $rows = array();

  $result = usertags_db_fetch_tag_collections();
  while ($record = $result->fetchAssoc()) {
    $edit_link = '<div>' . l(t('edit'), 'admin/structure/usertags/tag_collection/' . $record['vid'] . '/edit') . '</div>';
    $delete_link = '<div>' . l(t('delete'), 'admin/structure/usertags/tag_collection/' . $record['vid'] . '/delete') . '</div>';
    $list_tags_link = '<div>' . l(t('list tags'), 'admin/structure/usertags/tag_collection/' . $record['vid'] . '/list') . '</div>';

    $rows[] = array(
      $record['name'],
      $edit_link . $delete_link . $list_tags_link,
    );
  }


  $build['tablesort_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $build;
}

// -------------------------------------
//               Form
// -------------------------------------

/**
 *  Used both for add and edit widget. $vid is NULL when add is wanted
 */
function usertags_tag_collection_form($form, &$form_state, $vid = NULL) {
  if ($vid != NULL) {
    $record = usertags_db_get_tag_collection($vid);

    $form['vid'] = array(
      '#type' => 'hidden',
      '#default_value' => $vid,
    );
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => ($vid != NULL ? $record['name'] : ''),
    '#required' => TRUE,
  );

  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#default_value' => ($vid != NULL ? $record['machine_name'] : NULL),
    '#maxlength' => 255,
    '#machine_name' => array(
      'exists' => 'usertags_db_get_tag_collection_by_machine_name',
    ),
  );

  $form['title_page_stuff_you_tagged'] = array(
    '#type' => 'textfield',
    '#title' => t('Title of page showing content with a certain usertag (user)'),
    '#description' => t('The title of the page showing all stuff that user tagged with a certain tag - for this vocabulary. You can use %tagname as a placeholder for the tagname'),
    '#default_value' => ($vid != NULL ? $record['title_page_stuff_you_tagged'] : 'Stuff you tagged "%tagname"'),
    '#required' => FALSE,
  );

  $form['title_page_stuff_somebody_tagged'] = array(
    '#type' => 'textfield',
    '#title' => t('Title of page showing content with a certain usertag (community)'),
    '#description' => t('The title of the page showing all stuff that somebody tagged with a certain tag - for this vocabulary. You can use %tagname as a placeholder for the tagname'),
    '#default_value' => ($vid != NULL ? $record['title_page_stuff_somebody_tagged'] : 'Stuff somebody tagged "%tagname"'),
    '#required' => FALSE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => ($vid != NULL ? t('Update') : t('Add tag collection')),
  );

  return $form;
}

function usertags_tag_collection_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $is_edit = isset($values['vid']);

  if ($is_edit) {
    $query = db_update('usertags_tag_collection');
    $query->condition('vid', $values['vid'], '=');
  }
  else {
    $query = db_insert('usertags_tag_collection');
  }
  $query->fields(array(
    'name' => $values['name'],
    'machine_name' => $values['machine_name'],
    'title_page_stuff_you_tagged' => $values['title_page_stuff_you_tagged'],
    'title_page_stuff_somebody_tagged' => $values['title_page_stuff_somebody_tagged'],
  ));
  $result = $query->execute();

  $form_state['redirect'] = 'admin/structure/usertags/tag_collection';
}

// -------------------------------------
//               Delete
// -------------------------------------

function usertags_delete_tag_collection($vid) {
  drupal_set_title(t('Delete tag collection'));
  return drupal_get_form('usertags_delete_tag_collection_form', $vid);
}

function usertags_delete_tag_collection_form($form, &$form_state, $vid) {
  $form['vid'] = array(
    '#type' => 'hidden',
    '#default_value' => $vid,
  );
  // TODO: Warn if tag collection is being used (or maybe even dont allow deleting tag collections in use)
  return confirm_form($form, t('Are you sure you want to delete the tag collection?'), 'admin/structure/usertags/tag_collection', '', t('Delete'), t('Cancel'));
}

function usertags_delete_tag_collection_form_submit($form, &$form_state) {
  $vid = $form_state['values']['vid'];
  usertags_db_delete_tag_collection($vid);
  $form_state['redirect'] = 'admin/structure/usertags/tag_collection';
  return;
}



