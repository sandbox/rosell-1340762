<?php

/**
 * @file
 * Admin interface for managing Tags (this admin section is reached by going to Tag Collections and selecting "list tags")
 */

// -------------------------------------
//               List
// -------------------------------------

function usertags_tags_list_title($vid) {
  return t('Tags in Tag Collection: "@voc"', array('@voc' => usertags_db_get_tag_collection_name($vid)));
}

/**
 * Form for tag collections
 */
function usertags_tags_list($vid) {

  return theme('usertags_tags_list_form', array('form' => drupal_get_form('usertags_tags_list_form')));
//  return drupal_get_form('usertags_tags_list_form');
}

/**
 * Form for tag collections
 */
function usertags_tags_list_form($form, &$form_state, $vid) {
  $form['#tree'] = TRUE;

  $query = db_select('usertags_tag_data', 'td');
  $query->leftjoin('usertags_index', 'ti', 'td.tid = ti.tid');  //JOIN usertags_index with usertags_tag_data
  $query->fields('td', array('tid', 'name', 'weight'));
  $query->groupBy('td.tid');
  $query->addExpression('COUNT(ti.tid)', 'num');
  $query->orderBy('weight', 'ASC');
  $query->condition('td.vid', $vid, '=');
  $result = $query->execute();

//  $result = usertags_db_fetch_tags_in_tag_collection($vid, 'weight');
  while ($record = $result->fetchAssoc()) {
    $tid = $record['tid'];
    $name = $record['name'];
    $weight = $record['weight'];
    $num = $record['num'];

    $form['tags'][$tid]['name'] = array('#markup' => $name);
    $form['tags'][$tid]['weight'] = array(
      '#type' => 'weight',
      '#default_value' => $weight,
    );

    $form['tags'][$tid]['num'] = array('#markup' => $num);

    $edit_link = '<div>' . l(t('edit'), 'admin/structure/usertags/tag_collection/' . $vid . '/tag/' . $tid . '/edit') . '</div>';
    $delete_link = '<div>' . l(t('delete'), 'admin/structure/usertags/tag_collection/' . $vid . '/tag/' . $tid . '/delete') . '</div>';

    $form['tags'][$tid]['operations'] = array('#markup' => $edit_link . $delete_link);

  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));
  return $form;
}

function usertags_tags_list_form_submit($form, &$form_state) {
  foreach ($form_state['values']['tags'] as $tid => $data) {
    if (is_array($data) && isset($data['weight'])) {
      // Only update if this is a form element with weight.
      db_update('usertags_tag_data')
        ->fields(array('weight' => $data['weight']))
        ->condition('tid', $tid)
        ->execute();
    }
  }
  drupal_set_message(t('The tag ordering has been saved.'));
}

function theme_usertags_tags_list_form($variables) {
  $form = $variables['form'];

  $rows = array();
  if (isset($form['tags'])) {
    foreach (element_children($form['tags']) as $tid) {
      $form['tags'][$tid]['weight']['#attributes']['class'] = array('taglist-order-weight');
      $rows[] = array(
        'data' => array(
          drupal_render($form['tags'][$tid]['name']),
          drupal_render($form['tags'][$tid]['weight']),
          drupal_render($form['tags'][$tid]['num']),
          drupal_render($form['tags'][$tid]['operations']),
        ),
        'class' => array('draggable'),
      );
    }
  }
  $header = array(t('Name'), t('Weight'), t('Used number of times'), t('Operations'));
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'taglist-order')));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('taglist-order', 'order', 'sibling', 'taglist-order-weight');

  return $output;
}

// -------------------------------------
//               Add
// -------------------------------------

function usertags_add_tag($vid) {
  $voc_name = usertags_db_get_tag_collection_name($vid);
  drupal_set_title(t('Add new tag to Tag Collection: "@voc"', array('@voc' => $voc_name)));
  return drupal_get_form('usertags_add_tag_form', $vid);
}

function usertags_add_tag_form($form, &$form_state, $vid) {

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
  );

  $form['vid'] = array(
    '#type' => 'hidden',
    '#default_value' => $vid,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add tag'),
  );

  return $form;
}

function usertags_add_tag_form_submit($form, &$form_state) {

  $vid = $form_state['values']['vid'];
  $name = $form_state['values']['name'];

  $query = db_insert('usertags_tag_data');
  $query->fields(array(
    'vid' => $vid,
    'name' => $name,
  ));
  $result = $query->execute();

  $form_state['redirect'] = 'admin/structure/usertags/tag_collection/' . $vid . '/list';
}

// -------------------------------------
//               Edit
// -------------------------------------


function usertags_edit_tag($tid, $vid) {
  drupal_set_title(t('Edit tag'));
  return drupal_get_form('usertags_edit_tag_form', $tid, $vid);
}

function usertags_edit_tag_form($form, &$form_state, $tid, $vid) {
  $query = db_select('usertags_tag_data', 'ttd');
  $query->fields('ttd', array('tid', 'name', 'weight'));
  $query->condition('ttd.tid', $tid, '=');
  $result = $query->execute();
  $record = $result->fetchAssoc();

  $form['tid'] = array(
    '#type' => 'hidden',
    '#default_value' => $tid,
  );

  $form['vid'] = array(
    '#type' => 'hidden',
    '#default_value' => $vid,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => $record['name'],
    '#required' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  return $form;
}

function usertags_edit_tag_form_submit($form, &$form_state) {
  $tid = $form_state['values']['tid'];
  $vid = $form_state['values']['vid'];
  $name = $form_state['values']['name'];

  $query = db_update('usertags_tag_data');
  $query -> fields(array(
    'name' => $name,
  ));
  $query->condition('tid', $tid, '=');
  $result = $query->execute();

  $form_state['redirect'] = 'admin/structure/usertags/tag_collection/' . $vid . '/list';
}

// -------------------------------------
//               Delete
// -------------------------------------

function usertags_delete_tag($vid, $tid) {
  drupal_set_title(t('Delete tag'));
  return drupal_get_form('usertags_delete_tag_form', $vid, $tid);
}

function usertags_delete_tag_form($form, &$form_state, $vid, $tid) {
  $form['vid'] = array(
    '#type' => 'hidden',
    '#default_value' => $vid,
  );
  $form['tid'] = array(
    '#type' => 'hidden',
    '#default_value' => $tid,
  );
  // TODO: Warn if tag collection is being used (or maybe even dont allow deleting tag collections in use)
  return confirm_form($form, t('Are you sure you want to delete the tag?'), 'admin/structure/usertags/tag_collection/' . $vid . '/list', '', t('Delete'), t('Cancel'));
}

function usertags_delete_tag_form_submit($form, &$form_state) {
  $vid = $form_state['values']['vid'];
  $tid = $form_state['values']['tid'];

  $query = db_delete('usertags_index');
  $query->condition('tid', $tid, '=');
  $num_deleted = $query->execute();
//  dsm('deleted ' . $num_deleted . ' records in the index');

  $query = db_delete('usertags_tag_data');
  $query->condition('tid', $tid, '=');
  $num_deleted = $query->execute();

  $form_state['redirect'] = 'admin/structure/usertags/tag_collection/' . $vid . '/list';
  return;
}








