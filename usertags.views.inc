<?php

/**
 * @file
 * Defines views structures for Views integration
 */

/**
 * Implements hook_views_data().
 */
function usertags_views_data() {
  $data = array(
    'usertags_tag_data' => array(
      'table' => array(
        'group' => t('Usertags Tag'),
        'base' => array(
          'field' => 'tid',
          'title' => t('Usertags'),
        ),
        'join' => array(
          'usertags_tag_collection' => array(
            'left_field' => 'vid',
            'field' => 'vid',
          ),
          'usertags_index' => array(
            'left_field' => 'tid',
            'field' => 'tid',
          ),
        ),
      ),
      'tid' => array(
        'title' => t('Term ID'),
        'help' => t('The id of the Usertags Tag (tid)'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
        ),
      ),
      'name' => array(
        'title' => t('Tag name'),
        'help' => t('The name of the Tag'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
          'help' => t('Term name.'),
          'handler' => 'views_handler_argument_string',
        ),
      ),
    ),

    'usertags_tag_collection' => array(
      'table' => array(
        'group' => t('Usertags Tag Collection'),
        'join' => array(
          'usertags_tag_data' => array(
            'left_field' => 'vid',
            'field' => 'vid',
          ),
        ),
      ),
      'vid' => array(
        'title' => t('Tag Collection ID'),
        'help' => t('The id of the Usertags tag collection (vid)'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
        ),
      ),
      'name' => array(
        'title' => t('Tag Collection name'),
        'help' => t('The name of the Tag Collection'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
          'help' => t('Term name.'),
          'handler' => 'views_handler_argument_string',
        ),
      ),
    ),

    'usertags_index' => array(
      'table' => array(
        'group' => t('Usertags Tag Index'),
        'join' => array(
          'usertags_tag_data' => array(
            'left_field' => 'tid',
            'field' => 'tid',
          ),
          'node' => array(
            'left_field' => 'nid',
            'field' => 'cid',
          ),
          'comment' => array(
            'left_field' => 'cid',
            'field' => 'cid',
          ),
          'users' => array(
            'left_field' => 'uid',
            'field' => 'cid',
          ),
        ),
      ),
      'entity_type' => array(
        'title' => t('Entity type'),
        'help' => t('The entity type (ie node, comment, user)'),
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_string',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
      'cid' => array(
        'title' => t('Content ID'),
        'help' => t('The id of the content (it is the node id, if entity is node, etc)'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
        ),
        'relationship' => array(
          'title' => t('Comment'),
          'help' => t('Relate to the node that was tagged'),
          'base' => 'node',
          'base field' => 'nid',
          'relationship field' => 'cid',
          'label' => t('Usertags - Tagged Node'),
        ),
      ),
      'cid2' => array(
        'title' => t('Content ID'),
        'help' => t('The id of the comment'),
        'relationship' => array(
          'title' => t('Comment'),
          'help' => t('Relate to the comment that was tagged'),
          'base' => 'comment',
          'base field' => 'cid',
          'relationship field' => 'cid',
          'label' => t('Usertags - Tagged Comment'),
        ),
      ),
      'cid3' => array(
        'title' => t('Content ID'),
        'help' => t('The id of the user'),
        'relationship' => array(
          'title' => t('User'),
          'help' => t('Relate to the user that did the tagging'),
          'base' => 'users',
          'base field' => 'uid',
          'relationship field' => 'cid',
          'label' => t('Usertags - Tagged User'),
        ),
      ),
      'uid' => array(
        'title' => t('User ID'),
        'help' => t('The id of the user added the tag'),
        'field' => array(
          'handler' => 'views_handler_field_user',
          'click sortable' => TRUE,
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_user_uid',
          'name field' => 'name',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'relationship' => array(
          'title' => t('User'),
          'help' => t('Relate to the user that made the tag'),
          'base' => 'users',
          'base field' => 'uid',
          'relationship field' => 'uid',
          'label' => t('Usertags - User'),
        ),
      ),
      'tid' => array(
        'title' => t('Usertag Tag ID'),
        'help' => t('The ID of the Tag'),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_numeric',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_numeric',
        ),
        'relationship' => array(
          'title' => t('Tag'),
          'help' => t('Relate to the tag data (the tag name)'),
          'base' => 'usertags_tag_data',
          'base field' => 'tid',
          'relationship field' => 'tid',
          'label' => t('Usertag Tag'),
        ),
      ),
    ),
  );

  return $data;
}


