<?php

/**
 * @file
 * Admin page
 */

// -------------------------------------
//               List
// -------------------------------------

/**
 * Builds and returns the widgets list
 */
function usertags_widgets_list() {

  // example: http://api.drupal.org/api/examples/tablesort_example--tablesort_example.module/7/source
  // create a table
  $header = array(
    // The header gives the table the information it needs in order to make
    // the query calls for ordering. TableSort uses the field information
    // to know what database column to sort by.
//    array('data' => t('Numbers'), 'field' => 't.numbers'),
  //  array('data' => t('Letters'), 'field' => 't.alpha'),
    //array('data' => t('Mixture'), 'field' => 't.random'),
    array('data' => t('Widget')),
    array('data' => t('Widget Type')),
    array('data' => t('Tag Collection')),
    array('data' => t('Operations')),
  );

  $rows = array();

  $result = usertags_db_fetch_widgets();
  while ($record = $result->fetchAssoc()) {
    $edit_link = '<div>' . l(t('edit'), 'admin/structure/usertags/widget/' . $record['eid'] . '/edit') . '</div>';
    $delete_link = '<div>' . l(t('delete'), 'admin/structure/usertags/widget/' . $record['eid'] . '/delete') . '</div>';

    $rows[] = array(
      $record['name'],
      $record['widget_type'],
      $record['tv_name'],
      $edit_link . $delete_link,
    );
  }



  $build['tablesort_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $build;
}

// -------------------------------------
//               Add
// -------------------------------------

function usertags_add_widget() {
  drupal_set_title(t('Add new widget'));
  return drupal_get_form('usertags_add_widget_form');
}

/**
 *  Used both for add widget and edit widget. $eid is NULL when add is wanted
 */
function usertags_widget_form($form, $form_state, $eid) {
  if ($eid != NULL) {
    $record = usertags_db_get_widget($eid);

    $form['eid'] = array(
      '#type' => 'hidden',
      '#default_value' => $eid,
    );
  }

  // Name
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
  );

  // Machine name
  $form['machine_name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => 255,
    '#machine_name' => array(
      'exists' => 'usertags_db_get_widget_by_machine_name',
    ),
  );

  // Label
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#required' => FALSE,
    '#description' => 'Label. You may want to add ":" behind, as this is not automatically prepended',
  );

  // Label
  $form['label_style'] = array(
    '#type' => 'select',
    '#title' => t('Label style'),
    '#options' => array(0 => 'inline', 1 => 'above', 2 => 'hidden'),
    '#required' => FALSE,
    '#description' => '',
  );


  // Vid
  // Get tag collections
  $options = array();
  $result2 = usertags_db_fetch_tag_collections();
  while ($record2 = $result2->fetchAssoc()) {
    $options[$record2['vid']] = $record2['name'];
  }

  $form['vid'] = array(
    '#type' => 'select',
    '#title' => t('Tag Collection'),
    '#options' => $options,
    '#required' => TRUE,
  );

  // Weight
  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#delta' => 50,
    '#default_value' => 0,
    '#description' => t('The weight determines the position within the node/user/comment. Hint: When you can click "show row weights" in the "Manage display" tab in content types and set them manually in order to get control'),
  );


  // Widgets
  // Get all widget-types available
  $widgets = array();
  foreach (module_implements('get_usertags_widget_name') as $module) {
    $widget_name = module_invoke($module, 'get_usertags_widget_name');
    $widgets[$module] = $widget_name;
  }
  $form['widget_type'] = array(
    '#type' => 'select',
    '#title' => t('Widget type'),
    '#options' => $widgets,
    '#ajax' => array(
      'callback' => 'usertags_widgetchange_ajax_callback',
      'wrapper' => 'usertags_widgetchange_wrapper',
    ),
  );

  // Widget settings
  $form['widget_settings'] = array(
    '#type' => 'fieldset',
    '#title' => 'Widget specific settings',
    '#prefix' => '<div id="usertags_widgetchange_wrapper">',
    '#suffix' => '</div>',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $widget_type = '';

  if (isset($form_state['values'])) {
    $widget_type = $form_state['values']['widget_type'];
  }
  else {
    if ($eid != NULL) {
      $widget_type = $record['widget_type'];
    }
    else {
      // Set to the first widget in the combobox (the first key in the $widgets array)
      reset($widgets); // Resets the internal pointer of the array to its first widget
      $widget_type = key($widgets);   // Picks the key of current pointer in the $widgets array
    }
  }

  $fields = usertags_create_widget_form($widget_type);
  if ($fields != NULL) {
    foreach ($fields as $key => $value) {
      $form['widget_settings'][$key] = $value;
    }
  }

  if ($eid != NULL) {
    $widget_options = $record['widget_options'];
    $widget_values = usertags_get_widget_values($widget_type, $widget_options);
    if ($widget_values != NULL) {
      foreach ($fields as $key => $value) {
        if ($value['#type'] != 'fieldset') {
          $form['widget_settings'][$key]['#default_value'] = $widget_values[$key];
        }
        else {  // we allow fieldsets, but not nested
          foreach ($fields[$key] as $key2 => $value2) {
            if ($key2[0] != '#') {
              $form['widget_settings'][$key][$key2]['#default_value'] = $widget_values[$key2];
            }
          }
        }
      }
    }
  }

  // Widget settings
  $form['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => 'Visibility options',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Bundles
  $bundle_options = array();
  $entity_types = array('node', 'user', 'comment');
  foreach ($entity_types as $entity_type) {
    $info = entity_get_info($entity_type);
    foreach ($info['bundles'] as $key => $value) {
        $bundle_options[$entity_type . '|' . $key] = $value['label'];
    }
  }

  $form['visibility']['bundles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display on the following content:'),
    '#options' => $bundle_options,
  );

  // View modes
  $modes_options = array();
  foreach ($entity_types as $entity_type) {
    $info = entity_get_info($entity_type);
    foreach ($info['view modes'] as $key => $value) {
        $modes_options[$entity_type . '|' . $key] = $value['label'];
    }
  }

  $form['visibility']['view_modes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display on the following view modes:'),
    '#options' => $modes_options,
  );

  // Roles

  $form['visibility']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Display for the following roles:'),
    '#options' => user_roles(false),
  );

  // Submit
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Add widget',
  );

  if ($eid != NULL) {
    $form['name']['#default_value'] = $record['name'];
    $form['machine_name']['#default_value'] = $record['machine_name'];
    $form['vid']['#default_value'] = $record['vid'];
    $form['widget_type']['#default_value'] = $record['widget_type'];
    $form['visibility']['bundles']['#default_value'] = explode(',', $record['bundles']);
    $form['visibility']['view_modes']['#default_value'] = explode(',', $record['view_modes']);
    $form['visibility']['roles']['#default_value'] = explode(',', $record['roles']);
    $form['weight']['#default_value'] = $record['weight'];
    $form['label']['#default_value'] = $record['label'];
    $form['label_style']['#default_value'] = $record['label_style'];
    $form['submit']['#value'] = t('Update');
  }

  return $form;
}

function usertags_widget_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $widget_form = usertags_create_widget_form($values['widget_type']);
  foreach ($widget_form as $key => $value) {
    if ($value['#type'] != 'fieldset') {
      $widget_form_values[substr($key, 7)] = $form_state['values'][$key];
    }
    else { // ps: we allow fieldsets, but not nested
      foreach ($widget_form[$key] as $key2 => $value2) {
        if ($key2[0] != '#') {
          $widget_form_values[substr($key2, 7)] = $form_state['values'][$key2];
        }
      }
    }
  }
  $widget_options = drupal_json_encode($widget_form_values);

  if (isset($form_state['values']['eid'])) {
    $query = db_update('usertags_widget');
    $query->condition('eid', $form_state['values']['eid'], '=');
  }
  else {
    $query = db_insert('usertags_widget');
  }
  $query->fields(array(
    'name' => $values['name'],
    'machine_name' => $values['machine_name'],
    'vid' => $values['vid'],
    'widget_type' => $values['widget_type'],
    'widget_options' => $widget_options,
    'bundles' => _usertags_convert_options_to_string($values['bundles']),
    'view_modes' => _usertags_convert_options_to_string($values['view_modes']),
    'roles' => _usertags_convert_options_to_string($values['roles']),
    'weight' => $values['weight'],
    'label' => $values['label'],
    'label_style' => $values['label_style'],
  ));
  $result = $query->execute();
  $form_state['redirect'] = 'admin/structure/usertags';
}

function _usertags_convert_options_to_string($options_array) {
  $str = '';
  foreach ($options_array as $key => $value) {
    if ($value != '0') {
      if ($str != '') {
        $str .= ',';
      }
      $str .= $value;
    }
  }
  return $str;
}

// -------------------------------------
//               Delete
// -------------------------------------

function usertags_delete_widget($eid) {
  drupal_set_title(t('Delete widget'));
  return drupal_get_form('usertags_delete_widget_form', $eid);
}

function usertags_delete_widget_form($form, &$form_state, $eid) {
  $form['eid'] = array(
    '#type' => 'hidden',
    '#default_value' => $eid,
  );
  // TODO: Warn if widget is being used (or maybe even dont allow deleting widgets in use)
  return confirm_form($form, t('Are you sure you want to delete the widget?'), 'admin/structure/usertags', '', t('Delete'), t('Cancel'));
}

function usertags_delete_widget_form_submit($form, &$form_state) {
  db_delete('usertags_widget')
    ->condition('eid', $form_state['values']['eid'])
    ->execute();

  $form_state['redirect'] = 'admin/structure/usertags';
  return;
}

// -------------------------------------
//               Ajax
// -------------------------------------

function usertags_widgetchange_ajax_callback($form, $form_state) {
  // Label

/*
  $form['label2'] = array(
    '#type' => 'textfield',
    '#title' => t('Label 2:' . $form_state['values']['widgettype']),
    '#required' => FALSE,
    '#description' => 'Label. You may want to add ":" behind, as this is not automatically prepended',
  );
  */
  return $form['widget_settings'];
}


